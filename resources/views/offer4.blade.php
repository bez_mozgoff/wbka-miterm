<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chocolife</title>
    <link href="https://fonts.googleapis.com/css?family=Vollkorn:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<!--HEADER-->

<!---<header id="header" class="header nav-link">
    <div class="container">
        <div class="ml-auto">
            <nav>
                <ul class="menu d-flex justify-content-center">
                    <li class="menu__item">
                        <a class="link-color" href="#">
                            All items
                        </a>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Top rated
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Most sales
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            New
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Old
                        </a>
                    </li>
                    <li class="menu__item">
                        <a class="text-light" href="#">
                            Archive
                        </a>
                    </li>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>

    <!--STICKY NAVBAR-->

<nav id="nav" class="navbar navbar-expand-lg navbar-dark sticky-top bg-faded">
    <div class="container">
        <a class="link-logo" href="index.html">
            ChocoLogo
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <li class="nav-item dropdown my-auto">
                <a class="nav-link text-light small dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Choose your city <span class="badge badge-light">2</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Almaty</a>
                    <a class="dropdown-item" href="#">Astana</a>
                </div>
            </li>
            <form class="form-inline my-2 mx-md-auto my-lg-0">
                <input type="text" class="form-control mr-sm-2">
                <button class="btn btn-outline-color my-2 my-sm-0">Search</button>
            </form>
            <div class="ml-auto">
                <button type="button" class="btn text-color btn-link">Sign in</button>
                <button type="button" class="btn text-light btn-link">Sign up</button>
            </div>
        </div>
    </div>
</nav>

<!--jumbortron-->

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-4 text-center">Hawaii aquapark</h1>
        <p class="lead">Want to spend a day off in an exotic atmosphere, plenty to swim in crystal clear water and taste
            the dishes of tropical cuisine? To do this, you do not need to flee for a plane ticket and fly to the edge
            of the world. Every day, Hawaii Water Park in Almaty, one of the largest theme parks in Kazakhstan, opens
            its doors to water lovers.</p>
    </div>
</div>

<!--section action-->

<section>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 mt-4 col-md-12">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="assets/slider4-1.jpg" alt="First slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="assets/slider4-2.jpg" alt="Second slide">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="assets/slider4-3.jpg" alt="Third slide">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                   data-slide="prev">
                                    <span class="fas fa-chevron-circle-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                   data-slide="next">
                                    <span class="fas fa-chevron-circle-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-4">
                            <h3 class="card-title border-bottom">Offer conditions</h3>
                            <p class="text-dark card-text">For adults and children, the water park of Hawaii in Almaty
                                offers:
                                water slides - different in height and configuration, they will please visitors of all
                                ages.</p>
                            <p class="text-dark card-text">Fans of extreme can make a descent from the 20-meter hill,
                                and especially for small customers on the territory there is a zone with children's
                                attractions;</p>
                            <p class="text-dark card-text">SPA-zone - for relaxation and personal care in the complex
                                there are hamam and saunas, cryo-room, jacuzzi, aroma-shower and solarium;
                                cafeteria and bars - tropical cuisine and exotic cocktails, children's menu for young
                                guests will please all visitors.
                            </p>
                            <p class="text-dark card-text">Pools - crystal clear water, various depths and cascades of
                                waterfalls. Children will be very pleased with the wave pool, and for adults there is a
                                zone with a water massage;
                            </p>
                            <h1>990$</h1>
                            <button class="btn btn-outline-color2 my-2 my-sm-0"><i class="fas fa-shopping-cart"></i> Add
                                to basket
                            </button>
                            <button class="btn btn-outline-color2 my-2 my-sm-0"><i class="fas fa-credit-card"></i> Buy
                                now
                            </button>

                        </div>

                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col mt-4">
                                    <div class="card-body">
                                        <h5 class="text-dark text-center card-title border-bottom border-top border-dark">
                                            Offer description</h5>
                                        <p class="text-dark card-text">Want to spend a day off in an exotic atmosphere,
                                            plenty to swim in crystal clear water and taste the dishes of tropical
                                            cuisine? To do this, you do not need to flee for a plane ticket and fly to
                                            the edge of the world. Every day, Hawaii Water Park in Almaty, one of the
                                            largest theme parks in Kazakhstan, opens its doors to water lovers.</p>
                                        <p class="text-dark card-text">The complex is located in the modern shopping
                                            center Aport. This is a unique
                                            entertainment complex that offers customers a piece of the tropics at any
                                            time of the year. Several pools, extreme slides and entertainment for kids
                                            of all ages make it a holiday in a small family adventure.</p>
                                        <p class="text-dark card-text">A piece of real Hawaii in Almaty - a water park
                                            in the shopping complex Aport, will please you with a constant tropical
                                            temperature of 30 degrees, incendiary music and a lot of fun entertainment.
                                            The atmosphere of celebration and carefree reigns here all year round.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right card-footer">Last purchase 4 hours ago</div>
        </div>
    </div>
</section>

<!--footer-->

<footer class="mainfooter mt-4" role="contentinfo">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <!--Column1-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">Address</h4>
                        <address>
                            <ul class="list-unstyled">
                                <li>
                                    City Hall<br>
                                    Groove Street <br>
                                    California<br>
                                    635<br>
                                </li>
                                <li>Phone: +7-701-785-2022</li>
                            </ul>
                        </address>
                    </div>
                </div>
                <!--Column2-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">About us</h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Our company</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i> Instagram</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook"></i> Facebook</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-github"></i> Github</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Column3-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">Our aplications</h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">
                                    <i class="fab fa-android"></i> Android</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-apple"></i> IOS</a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-windows"></i> Windows Phone</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Column4-->
                <div class="col-md-3 mb-4 col-sm-6">
                    <div class="footer-pad">
                        <h4 class="border-bottom border-dark">For partners</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Popular departments</a></li>
                            <li><a href="#">For your business</a></li>
                            <li><a href="#">Popular services</a></li>
                            <li><a href="#">Get started</a></li>
                            <li><a class="pl-2 nav-link p-0" href="#">
                                <i class="fab fa-medapps"></i>
                                Need help?</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer Bottom-->

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-center">&copy; Copyright 2018 - Almaty. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>